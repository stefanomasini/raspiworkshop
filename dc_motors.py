import RPi.GPIO as GPIO
import math, time
from time import sleep
 
PINS = {
    'A': (24, 23),  # Blue wire, orange wire
    'B': (22, 17),  # Blue wire, orange wire
}

period_ms = 20.0

pwm = {
    'A': [None, None],
    'B': [None, None],
}
 
print "Setting up GPIO"
GPIO.setmode(GPIO.BCM)
for motor, motor_pins in PINS.items():
    for offset in (0, 1):
        GPIO.setup(motor_pins[offset], GPIO.OUT)
        p = GPIO.PWM(motor_pins[offset], 1000.0 / period_ms)
        pwm[motor][offset] = p
        p.start(0)

def motor_forward(motor, power):
    pwm[motor][0].ChangeDutyCycle(power)
    pwm[motor][1].ChangeDutyCycle(0)

def motor_backward(motor, power):
    pwm[motor][0].ChangeDutyCycle(0)
    pwm[motor][1].ChangeDutyCycle(power)

def move(speed, direction):
    #print 'MOVE', speed, direction
    if (direction == 0):
        speedA = speed
        speedB = speed
    elif (direction < 0):
        speedA = -speed
        speedB = speed
    elif (direction > 0):
        speedA = speed
        speedB = -speed
    if speedA > 0:
        motor_forward('A', speedA)
    else:
        motor_backward('A', -speedA)
    if speedB > 0:
        motor_forward('B', speedB)
    else:
        motor_backward('B', -speedB)

def forward(power=100):
    move(power, 0)
    
def turn_left(power=50):
    move(power, -1)
    
def turn_right(power=50):
    move(power, 1)
    
def back(power=100):
    move(-power, 0)
    
def stop():
    move(0, 0)
    

#move(50, 0)
#time.sleep(2)
#move(100, 0)
#time.sleep(2)
#move(-100, 0)
#time.sleep(2)
#move(100, -1)
#time.sleep(2)
#move(100, 1)
#time.sleep(2)

if __name__ == '__main__':
    from getchar import getch
    while True:
        ch = getch()
        print repr(ch)
        if ch == 'i':
            forward()
        elif ch == 'j':
            turn_left()
        elif ch == 'l':
            turn_right()
        elif ch == 'k':
            back()
        elif ch == 'u':
            stop()
        elif ch == 'q':
            break

    GPIO.cleanup()

