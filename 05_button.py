import RPi.GPIO as GPIO, time, os      

GPIO.setmode(GPIO.BCM)
SENSOR_PIN = 10

GPIO.setup(SENSOR_PIN, GPIO.IN)
 
def readButton():
    return GPIO.input(SENSOR_PIN) == GPIO.HIGH

lastReading = None
while True:                                     
    value = readButton()
    if value != lastReading:
        print "high" if value else "low"
    lastReading = value
    time.sleep(0.01)

