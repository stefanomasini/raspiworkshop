import RPi.GPIO as GPIO, time, os      

GPIO.setmode(GPIO.BCM)
SENSOR_PIN = 9

GPIO.cleanup()

 
def readSensor():
    reading = 0
    GPIO.setup(SENSOR_PIN, GPIO.OUT)
    GPIO.output(SENSOR_PIN, GPIO.LOW)
    time.sleep(0.1)
 
    GPIO.setup(SENSOR_PIN, GPIO.IN)
    # This takes about 1 millisecond per loop cycle
    while (GPIO.input(SENSOR_PIN) == GPIO.LOW):
        reading += 1
    return max(2000 - reading, 0)


# Compute running average to stabilize the readings
def readLight(num_readings):
    readings = []
    while True:                                     
        value = readSensor()
        readings.append(value)
        if len(readings) > num_readings:
            readings.pop(0)
        average = sum(readings) / len(readings)
        yield average


def averageReads(num_readings):
    return sum(readSensor() for i in xrange(num_readings)) / num_readings


if __name__ == '__main__':
    #for v in readLight(5):
        #print v

    while True:
        print averageReads(5)
