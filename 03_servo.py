import RPi.GPIO as GPIO
import math, time
from time import sleep
 
LED_PIN = 25
 
print "Setting up GPIO"
GPIO.setmode(GPIO.BCM)
GPIO.setup(LED_PIN, GPIO.OUT)

period_ms = 20.0

p = GPIO.PWM(LED_PIN, 1000.0 / period_ms)

p.start(1.5 / period_ms * 100.0)

num_cycles = 5
cycle_duration_ms = 3000
update_freq = 50

ts = time.time()
while True:
    elapsed_ms = (time.time() - ts) * 1000.0
    cycle = elapsed_ms / float(cycle_duration_ms)
    if cycle > num_cycles:
        break
    angle = cycle * 2.0 * math.pi
    pulse_width_ms = 1.5 + math.cos(angle) * 0.5
    p.ChangeDutyCycle(pulse_width_ms / period_ms * 100.0)
    sleep(1.0 / update_freq)


GPIO.cleanup()

