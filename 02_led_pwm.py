import RPi.GPIO as GPIO
import math, time
from time import sleep
 
LED_PIN = 4
 
print "Setting up GPIO"
GPIO.setmode(GPIO.BCM)
GPIO.setup(LED_PIN, GPIO.OUT)

p = GPIO.PWM(LED_PIN, 100) # 75 Hz to "rest" the eye

p.start(0)

num_cycles = 5
cycle_duration_ms = 1000
update_freq = 50 # 25 frames per second is what hollywood tought us

ts = time.time()
while True:
    elapsed_ms = (time.time() - ts) * 1000.0
    cycle = elapsed_ms / float(cycle_duration_ms)
    if cycle > num_cycles:
        break
    p.ChangeDutyCycle(50 - math.cos(cycle * 2.0 * math.pi) * 49)
    sleep(1.0 / update_freq)

GPIO.cleanup()

