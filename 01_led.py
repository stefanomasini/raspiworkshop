import RPi.GPIO as GPIO
from time import sleep
 
LED_PIN = 4
 
print "Setting up GPIO"
GPIO.setmode(GPIO.BCM)
GPIO.setup(LED_PIN, GPIO.OUT)
 
def enable_led(should_enable):
    print "LED is " + ("ON" if should_enable else "OFF")
    GPIO.output(LED_PIN, 1 if should_enable else 0)
 
enable_led(False)
sleep(2)
enable_led(True)
sleep(2)
enable_led(False)
sleep(2)
enable_led(True)
sleep(2)
GPIO.cleanup()

