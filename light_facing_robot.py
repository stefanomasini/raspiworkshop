import RPi.GPIO as GPIO
import time
from light_sensor import averageReads
from dc_motors import turn_left, turn_right, stop

threshold = 10

def read():
    print 'reading...'
    light = averageReads(20)
    print 'light:', light
    return light


def keepReading():
    light_value = read()
    while True:
        new_light_value = read()
        if new_light_value > light_value + threshold:
            yield 'HIGHER', new_light_value
        elif new_light_value < light_value - threshold:
            yield 'LOWER', new_light_value
        else:
            yield 'EQUAL', new_light_value
        light_value = new_light_value

reader = keepReading()


def readUntilItStartsIncreasing(inBetweenReadsAction):
    while True:
        inBetweenReadsAction()
        way, amount = reader.next()
        print '  light change:', way
        if way == 'HIGHER':
            break

        
def readUntilItStartsDecreasing(inBetweenReadsAction):
    while True:
        inBetweenReadsAction()
        way, amount = reader.next()
        if way == 'LOWER':
            break


def turn_slightly_right():
    turn_right(100)
    time.sleep(0.3)
    stop()
        
def turn_slightly_left():
    turn_right(100)
    time.sleep(0.3)
    stop()
        

if __name__ == '__main__':

    print 'Search light to right'
    readUntilItStartsIncreasing(turn_slightly_right)
    print 'Search darkness to right'
    readUntilItStartsDecreasing(turn_slightly_right)
    print 'Search light again to left'
    readUntilItStartsIncreasing(turn_slightly_left)
    stop()
    
    GPIO.cleanup()
